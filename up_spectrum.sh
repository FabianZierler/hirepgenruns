# rename old name of the option to enable non-degenerate fermions
sed "s/def_semwall_2masses/def_semwall_nondeg/" -i ./input_spectrum
# add a line that stores the listfile of configurations
echo "mes:configlist = list_conf.txt" >> ./input_spectrum
# remove old option of passing listfiles
sed 's/-l $lfile/ /' -i run_spectrum_node
# insert a replacement rule for passing the inputfiles for multiple measurements
# step1: find correct line to insert in file
# step2: insert the code for a replacement using sed
# step3: when the run_spectrum_node is executed the sed command will  be triggered and
#        the correct listfile will be written in the input file
l=$(sed -n "/code/=" run_spectrum_node)
awk -v p='    sed "/mes:configlist/c\\mes:configlist = $lfile"  -i $infile' "NR==$l{print p }1" run_spectrum_node > tmp
mv tmp run_spectrum_node
# allow this file be executed by the user
chmod 744 run_spectrum_node
