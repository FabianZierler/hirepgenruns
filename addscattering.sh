#!/bin/bash

path2genruns="./../../hirepgenruns"

cp $path2genruns/runfiles/run_scattering_node ./run_scattering_node
sed "
    /mes:masses*/c\\$(grep mes:masses input_spectrum)
    /GLB_T/c\\$(grep GLB_T input_spectrum)
    /GLB_X/c\\$(grep GLB_X input_spectrum)
    /GLB_Y/c\\$(grep GLB_Y input_spectrum)
    /GLB_Z/c\\$(grep GLB_Z input_spectrum)
    " $path2genruns/infiles/input_scattering > ./input_scattering
# move a scripts for running spectrum into same directory
sed "
    /#SBATCH -D/c\\$(grep '#SBATCH -D' slurm_spectrum_node)
    /#SBATCH -J/c\\$(grep '#SBATCH -J' slurm_spectrum_node)    
    " $path2genruns/batchfiles/slurm_node_scattering > ./slurm_scattering_node
sed -i "s spectrum scattering " ./slurm_scattering_node 