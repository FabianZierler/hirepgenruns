#!/bin/bash
# this script needs to be run in the same diractory as the other input files

# obtain the group from the slurm file
# first match the line that contains the contains the gauge group
# then match the coloumn that specifies the run directory
# extract the group from that directoy
group=$(awk '/code/' run | awk 'BEGIN {RS="/"}/code/{print}' | tr -d " \t\r" | sed s/MPI// | sed s/code//)
if [[ $group == "" ]]; then
    group="Sp4"
fi

# copy from input_(r)hmc
# first select correspoinding line with awk '/pattern/ file'
# then split the file at the equal sign using the file separator =
# lastly trim all whitespace excpet line breaks
Lt=$(awk '/GLB_T/' input_hmc | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")  # temporal
Ls=$(awk '/GLB_X/' input_hmc | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")  # spatial
Nt=$(awk '/NP_T/' input_hmc  | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")
Nx=$(awk '/NP_X/' input_hmc  | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")
Ny=$(awk '/NP_Y/' input_hmc  | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")
Nz=$(awk '/NP_Z/' input_hmc  | awk 'BEGIN { FS="=" } {print $2}' | tr -d " \t\r")

let cores=$Nt*$Nx*$Ny*$Nz
# generate jobname/directoryname
# this assumes that the script is called in the directory of input_r(hmc)/slurm/etc....
dname=$(basename $PWD)
dpath=$PWD
# find absolute paths of hirepgenruns/ and the directory of input-files/run-files
# only consider two options
#   1) we are already in the directory within hirepgenrus
#      the directory hirepgenruns can then always be reached by 'cd ..'
#   2) we are in a directory containing the inputfiles
#      the directory hirepgenruns can then always be reached by 'cd ../../hirepgenruns'
if [[ $(basename $(dirname $PWD)) == "hirepgenruns" ]]; then
    cd ..
else
    cd ../../hirepgenruns
fi
echo $PWD

# sed replacement rules
echo "
/GLB_T/c\GLB_T = $Lt
/GLB_X/c\GLB_X = $Ls
/GLB_Y/c\GLB_Y = $Ls
/GLB_Z/c\GLB_Z = $Ls
/#SBATCH -D/c\#SBATCH -D /gpfs/data/fs71564/zierler/HiRep/runs$group/$dname
/cd/c\/gpfs/data/fs71564/zierler/HiRep/runs$group/$dname
" > sedfile
# extra replacement rules for MPI usage
echo "
    /NP_T/c\NP_T = $Nt
    /NP_X/c\NP_X = $Nx
    /NP_Y/c\NP_Y = $Ny
    /NP_Z/c\NP_Z = $Nz
" > sedfile2
cat sedfile sedfile2 > sedfileMPI
# move input files for scattering, spectrum and disconnected
sed -f sedfileMPI infiles/input_flow > $dpath/input_flow
# create slurm files for submitting jobs
sed " /#SBATCH -J/c\#SBATCH -J $dname.flow" batchfiles/slurm_flow > $dpath/slurm_flow
sed -f sedfile -i $dpath/slurm_flow
# set MPI options for generating gauge configurations
# and move scripts for running spectrum ans scattering measurements into same directory
if [ $group != "Sp4" ]; then
    sed "s \/code\/ \/code$group\/ " runfiles/run_flow > $dpath/run_flow
    if [ $cores > 1 ]; then
        sed -i "s \/code$group\/ \/code${group}MPI\/ " $dpath/run_flow
        sed -i "/code/s/^/mpirun -np $cores /"         $dpath/run_flow
    fi
elif [ $group = "Sp4" ]; then
    cp runfiles/run_flow $dpath/run_flow
    if [[ $cores > 1 ]]; then
        sed -i "s \/code\/ \/codeMPI\/ "       $dpath/run_flow
        sed -i "/code/s/^/mpirun -np $cores /" $dpath/run_flow
    fi
fi
# now replace set the number of cores in the slurm file
if [[ $cores < 48 ]]; then
    sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dpath/slurm_flow
elif [[ $cores == 48 ]]; then
    sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dpath/slurm_flow
else
    echo "Error: Multiple nodes are not taken care of in this script!"
fi
# mark all runfiles as executable
chmod 744 $dpath/run_flow
# done remove temp file 'sedfile'
rm sedfile sedfile2 sedfileMPI
