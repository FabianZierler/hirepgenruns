BEGIN {monomial = 0; id=-1}
{
    # check if parameters mass and monomial id are provided
    # otherwise cause an error    
    if (mass=="" || mid=="") { err = 0}
    else {err = 1}
    err = 1/err

    # check if line has been altered
    mod = 0

    # detect monomial and determine id of monomial
    if  ($1 ~ "monomial {") { monomial = 1 }
    if  ($1 ~ "}") { monomial = 0; id = -1}
    if  (monomial == 1 && $1 ~ "id") {id = $2}
    
    # assumes that monomial has the id 'mid'
    if  (id == mid && monomial == 1 && $1 ~ "mass" ){
        print $1"=" mass
        mod = 1
    }

    # print original line if unaltered
    if (mod == 0) { print $0 }
}
END {}
