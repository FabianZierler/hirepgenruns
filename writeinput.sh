#!/bin/bash

# algorithm: either "hmc" or "rhmc"
# Use hmc  for     degenerate masses
# Use rhmc for non-degenerate masses
alg="hmc"
# either SU2/3 or "Sp4"
group="Sp4"

# lattice size
Lt=36  # temporal
Ls=24  # spatial
# partition of the lattice for MPI (overlap is possible)
# (i.e. Nt does NOT need to be a divisor of Lt )
Nt=6
Nx=2
Ny=2
Nz=2
# compute number of cores needed
# on VSC every node has 48 cores which the ideal number of cores
# This script does not yet take care for cores > 48
# in that case one needs to make sure that only multiples of 48 cores
# are allowed. Everything else would just waste ressources
let cores=$Nt*$Nx*$Ny*$Nz

# inverse coupling
beta=7.05

# other parameters
tlen=1         # length of MD time
lastconf=1     # stop at this configuration
save=1         # save frequency of the configuration

# combination of quark masses
m1s=("-0.867")

# loop over list of quark masses
for ((i=0; i<${#m1s[@]}; i++)); do
    # read out masses from array specified above
    m1=${m1s[i]}
    m2=${m1s[i]}

    # generate jobname/directoryname
    dirname="Lt"$Lt"Ls"$Ls"beta"$beta"m1"$m1"m2"$m2
    mkdir $dirname

    # sed replacement rules
    echo "
        /GLB_T/c\GLB_T = $Lt
        /GLB_X/c\GLB_X = $Ls
        /GLB_Y/c\GLB_Y = $Ls
        /GLB_Z/c\GLB_Z = $Ls
        /mes:masses/c\mes:masses = $m1;$m2
        /disc:mass/c\disc:mass = $m1;$m2
        /beta =/c\ \tbeta = $beta
        /last conf/c\last conf = $lastconf
        /save freq/c\save freq = $save
        /tlen/c\tlen = $tlen
        /#SBATCH -D/c\#SBATCH -D /gpfs/data/fs71564/zierler/HiRep/runs$group/$dirname
        /cd/c\/gpfs/data/fs71564/zierler/HiRep/runs$group/$dirname
    " > sedfile
    # extra replacement rules for MPI usage
    echo "
        /NP_T/c\NP_T = $Nt
        /NP_X/c\NP_X = $Nx
        /NP_Y/c\NP_Y = $Ny
        /NP_Z/c\NP_Z = $Nz
    " > sedfile2
    cat sedfile sedfile2 > sedfileMPI
    # generate input files and save in sepcific directory
    # this assumes that for rhmc the fermion monomials have id 1 and 2
    #                        hmc the fermion monomial  has  id 1
    # set all the variables specified above in the input files
    if [ $alg = "hmc" ]; then
        mawk -F= -v mass=$m1 -v mid=1 -f monmass.awk infiles/input_hmc > $dirname/input_hmc
        sed -f sedfileMPI $dirname/input_hmc -i
    elif [ $alg = "rhmc" ]; then
        mawk -F= -v mass=$m1 -v mid=1 -f monmass.awk infiles/input_rhmc > $dirname/tmp
        mawk -F= -v mass=$m2 -v mid=2 -f monmass.awk $dirname/tmp > $dirname/input_rhmc
        sed -f sedfileMPI $dirname/input_rhmc -i
        rm  $dirname/tmp
    fi
    # move input files for scattering, spectrum and disconnected
    sed -f sedfile infiles/input_spectrum_node > $dirname/input_spectrum_node
    sed -f sedfileMPI infiles/input_spectrum > $dirname/input_spectrum
    sed -f sedfileMPI infiles/input_scattering > $dirname/input_scattering
    sed -f sedfileMPI infiles/input_scattering_I1 > $dirname/input_scattering_I1
    sed -f sedfileMPI infiles/input_disconnected > $dirname/input_disconnected
    # move a script for running hmc or rhmc into same directory
    if [ $alg = "rhmc" ]; then
        sed "s/input_hmc/input_rhmc/; s/out_hmc/out_rhmc/" runfiles/run > $dirname/run
    elif [ $alg = "hmc" ]; then
        sed "s/input_rhmc/input_hmc/; s/out_rhmc/out_hmc/" runfiles/run > $dirname/run
    fi
    # create slurm files for submitting jobs
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.configs"        batchfiles/slurm               > $dirname/slurm
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.spectrum "      batchfiles/slurm_spectrum      > $dirname/slurm_spectrum
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.scattering "    batchfiles/slurm_scattering    > $dirname/slurm_scattering
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.scattering_I1 " batchfiles/slurm_scattering_I1 > $dirname/slurm_scattering_I1
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.disconnected "  batchfiles/slurm_disconnected  > $dirname/slurm_disconnected
    sed " /#SBATCH -J/c\#SBATCH -J $dirname.spectrum_node " batchfiles/slurm_spectrum_node > $dirname/slurm_spectrum_node
    sed -f sedfile -i $dirname/slurm
    sed -f sedfile -i $dirname/slurm_spectrum
    sed -f sedfile -i $dirname/slurm_scattering
    sed -f sedfile -i $dirname/slurm_scattering_I1
    sed -f sedfile -i $dirname/slurm_disconnected
    sed -f sedfile -i $dirname/slurm_spectrum_node
    # set MPI options for generating gauge configurations
    # and move scripts for running spectrum ans scattering measurements into same directory
    if [ $group != "Sp4" ]; then
        sed "s \/code\/ \/code$group\/ " $dirname/run -i
        sed "s \/code\/ \/code$group\/ " runfiles/run_spectrum      > $dirname/run_spectrum
        sed "s \/code\/ \/code$group\/ " runfiles/run_scattering    > $dirname/run_scattering
        sed "s \/code\/ \/code$group\/ " runfiles/run_scattering_I1 > $dirname/run_scattering_I1
        sed "s \/code\/ \/code$group\/ " runfiles/run_disconnected  > $dirname/run_disconnected
        sed "s \/code\/ \/code$group\/ " runfiles/run_spectrum_node > $dirname/run_spectrum_node
        if [ $cores > 1 ]; then
            sed -i "s \/code$group\/ \/code${group}MPI\/ "  $dirname/run
            sed -i "/code/s/^/mpirun -np $cores /"  $dirname/run
            sed -i "s \/code$group\/ \/code${group}MPI\/ "  $dirname/run_spectrum
            sed -i "/code/s/^/mpirun -np $cores /"  $dirname/run_spectrum
            sed -i "s \/code$group\/ \/code${group}MPI\/ "  $dirname/run_scattering
            sed -i "s \/code$group\/ \/code${group}MPI\/ "  $dirname/run_scattering_I1
            sed -i "/code/s/^/mpirun -np $cores /"  $dirname/run_scattering
            sed -i "s \/code$group\/ \/code${group}MPI\/ "  $dirname/run_disconnected
            sed -i "/code/s/^/mpirun -np $cores /"  $dirname/run_disconnected
        fi
    elif [ $group = "Sp4" ]; then
        cp runfiles/run_spectrum        $dirname/run_spectrum
        cp runfiles/run_scattering      $dirname/run_scattering
        cp runfiles/run_scattering_I1   $dirname/run_scattering_I1
        cp runfiles/run_disconnected    $dirname/run_disconnected
        cp runfiles/run_spectrum_node   $dirname/run_spectrum_node
        if [[ $cores > 1 ]]; then
            sed -i "s \/code\/ \/codeMPI\/ "       $dirname/run
            sed -i "/code/s/^/mpirun -np $cores /" $dirname/run
            sed -i "s \/code\/ \/codeMPI\/ "       $dirname/run_spectrum
            sed -i "/code/s/^/mpirun -np $cores /" $dirname/run_spectrum
            sed -i "s \/code\/ \/codeMPI\/ "       $dirname/run_scattering
            sed -i "/code/s/^/mpirun -np $cores /" $dirname/run_scattering
            sed -i "s \/code\/ \/codeMPI\/ "       $dirname/run_scattering_I1
            sed -i "/code/s/^/mpirun -np $cores /" $dirname/run_scattering_I1
            sed -i "s \/code\/ \/codeMPI\/ "       $dirname/run_disconnected
            sed -i "/code/s/^/mpirun -np $cores /" $dirname/run_disconnected
        fi
    fi
    # now replace set the number of cores in the slurm file
    if [[ $cores < 48 ]]; then
        sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dirname/slurm
        sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dirname/slurm_spectrum
        sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dirname/slurm_scattering
        sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dirname/slurm_scattering_I1
        sed -i "/#SBATCH -n 1/c\#SBATCH -n $cores " $dirname/slurm_disconnected
    elif [[ $cores == 48 ]]; then
        sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dirname/slurm
        sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dirname/slurm_spectrum
        sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dirname/slurm_scattering
        sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dirname/slurm_scattering_I1
        sed -i "/#SBATCH -n 1/c\#SBATCH -N 1" $dirname/slurm_disconnected
    else
        echo "Error: Multiple nodes are not taken care of in this script!"
    fi
    # create directories for outputfiles and configs
    mkdir $dirname/configs
    mkdir $dirname/out
    # mark all runfiles as executable
    chmod 744 $dirname/run $dirname/run_spectrum $dirname/run_scattering $dirname/run_disconnected $dirname/run_spectrum_node
    # done remove temp file 'sedfile'
    rm sedfile sedfile2 sedfileMPI
done
# add filse for measuring the Wilson flow 
#cd $dirname
#bash ../add_flow.sh
