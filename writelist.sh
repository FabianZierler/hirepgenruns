#!/bin/bash
start=${1}
stop=${2}
# hack to include the case where a==confignumber
let a=$start-1
let b=$stop

awk -v start=$a -v stop=$b 'BEGIN {FS="n"};{ if( ($NF > start) && ($NF < stop) ){print $0} }'
